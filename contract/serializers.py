from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from products.models import Products
from .models import Client, Purchase, Contract, PayCheck


class PurchaseSerializer(serializers.ModelSerializer):
    total_sum = serializers.SerializerMethodField()

    class Meta:
        model = Purchase
        fields = ('contract', 'products', 'price', 'amount', 'date', 'total_sum')

    def get_total_sum(self, obj):
        return obj.get_total_sum

    def create(self, validated_data):
        amount = validated_data['amount']
        product = Products.objects.filter(id=validated_data['products'].id).first()
        if amount > product.amount:
            raise ValidationError('Not enough amount of products')
        else:
            pass
        return Purchase.objects.create(**validated_data)


class ContractSerializer(serializers.ModelSerializer):
    purchases = serializers.SerializerMethodField()

    class Meta:
        model = Contract
        fields = ('id', 'client', 'staff', 'purchases')
        read_only_fields = ('purchases',)

    def get_purchases(self, obj):
        query_set = obj.purchase_set.all()
        serializer = PurchaseSerializer(data=query_set, many=True)
        serializer.is_valid()
        return serializer.data


class ClientSerializer(serializers.ModelSerializer):
    client_contracts = serializers.SerializerMethodField()

    class Meta:
        model = Client
        fields = ('id', 'name', 'phone_number', 'date_added', 'debt', 'client_contracts',)
        read_only_fields = ['debt', 'client_contracts']

    def get_client_contracts(self, obj):
        query_set = obj.client_contracts.all()
        serializer = ContractSerializer(data=query_set, many=True)
        serializer.is_valid()
        return serializer.data


class PayCheckSerializer(serializers.ModelSerializer):
    client_purchases = PurchaseSerializer(source='clients.contracts.purchases', many=True)

    class Meta:
        model = PayCheck

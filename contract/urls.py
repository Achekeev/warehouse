from django.urls import path
from .views import ClientAPIView, PurchaseAPIView,  ClientDetailAPIView, PurchaseDetailView, ContractAPIView, \
    ContractDetailApiView, PayCheckAPIView

urlpatterns = [
    path('clients/', ClientAPIView.as_view(), name='clients'),
    path('clients/<int:pk>/', ClientDetailAPIView.as_view(), name='client details'),
    path('contracts/', ContractAPIView.as_view(), name='contract'),
    path('contracts/<int:pk>/', ContractDetailApiView.as_view(), name='contract-detail'),
    path('purchase/', PurchaseAPIView.as_view(), name='purchase'),
    path('purchase/<int:pk>/', PurchaseDetailView.as_view(), name='purchase_detail'),
    path('paycheck/', PayCheckAPIView.as_view(), name='paycheck')
]

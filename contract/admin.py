from django.contrib import admin

from products.models import Products
from .models import Client, Purchase, Contract


@admin.register(Purchase)
class PurchaseModelAdmin(admin.ModelAdmin):
    list_display = ('products', 'amount', 'price', 'total_sum')
    readonly_fields = ('total_sum',)

    def total_sum(self, obj):
        if obj.amount and obj.price:
            return obj.get_total_sum
        else:
            return 0

    def has_change_permission(self, request, obj=None, **kwargs):
        if obj is None:
            return True
        else:
            return False


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    readonly_fields = ('debt',)


admin.site.register(Contract)
from products.models import Products
from .models import Client, Purchase, Contract, PayCheck
from .serializers import ClientSerializer, PurchaseSerializer, ContractSerializer, PayCheckSerializer
from rest_framework import generics


class ContractAPIView(generics.ListCreateAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer


class ContractDetailApiView(generics.RetrieveAPIView):
    queryset = Contract.objects.all()
    serializer_class = ContractSerializer

    # def get_serializer_context(self):
    #     context = super(ContractDetailApiView, self).get_serializer_context()
    #     context.update({
    #         'user': self.request.user
    #     })
    #     return context


class ClientAPIView(generics.ListCreateAPIView):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class ClientDetailAPIView(generics.RetrieveAPIView):
    queryset = Client.objects.filter()
    serializer_class = ClientSerializer

    def get_serializer_context(self):
        context = super(ClientDetailAPIView, self).get_serializer_context()
        context.update({
            'user': self.request.user
        })
        return context


class PurchaseAPIView(generics.ListCreateAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer


class PurchaseDetailView(generics.RetrieveAPIView):
    queryset = Purchase.objects.all()
    serializer_class = PurchaseSerializer

    def get_serializer_context(self):
        context = super(PurchaseDetailView, self).get_serializer_context()
        context.update({
            'user': self.request.user
        })
        return context


class PayCheckAPIView(generics.ListCreateAPIView):
    queryset = PayCheck.objects.all()
    serializer_class = PayCheckSerializer




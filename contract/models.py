from django.db import models
from products.models import Products
from users.models import Staff


class Contract(models.Model):
    client = models.ForeignKey('Client', verbose_name='Заказчик', related_name='client_contracts',
                               on_delete=models.CASCADE)
    staff = models.ForeignKey('users.Staff', verbose_name='Ответственное лицо', on_delete=models.CASCADE)

    def __str__(self):
        return self.client.name

    class Meta:
        verbose_name = 'Contract'
        verbose_name_plural = 'Contracts'


class Client(models.Model):
    name = models.CharField(verbose_name='Имя', max_length=255)
    phone_number = models.CharField(verbose_name='Контактный телефон', max_length=255)
    date_added = models.DateTimeField(auto_now_add=True, verbose_name='Дата Регистрации')
    debt = models.PositiveIntegerField(verbose_name='Сумма Долга', default=0)

    def __str__(self):
        return self.name


class Purchase(models.Model):
    contract = models.ForeignKey('Contract', verbose_name='Контракт', on_delete=models.CASCADE, default='')
    products = models.ForeignKey('products.Products', verbose_name='Продукция', on_delete=models.CASCADE)
    amount = models.PositiveIntegerField(verbose_name='Количество', null=True, default=0)
    price = models.PositiveIntegerField(verbose_name='Цена', default=0,)
    date = models.DateTimeField(verbose_name='Дата', auto_now=True)


    @property
    def get_total_sum(self):
        return self.price * self.amount

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if self.pk is None:
            self.contract.client.debt += self.get_total_sum
            self.contract.client.save()

        save = super(Purchase, self).save()

    def __str__(self):
        return self.contract.client.name


class PayCheck(models.Model):
    client = models.ForeignKey('Client', verbose_name='Заказчик', related_name='client_paycheck',
                               on_delete=models.CASCADE)
# Create your models here.

    def __str__(self):
        return self.client
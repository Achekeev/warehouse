from django.contrib import admin
from .models import Category, Color, Model, Products


# class ProductModelFilter(admin.SimpleListFilter):
#     title = 'Модели'
#     parameter_name = 'Model'
#
#     def lookups(self, request, model_admin):
#         if 'category_products_model_id' in request.GET:
#             model_id = request.GET['category_products_model_id']
#             categories = set(([m.category for c in model_admin.model.objects.all()]))
#             print(categories)
#             return [()]
admin.site.register(Model)
admin.site.register(Category)
admin.site.register(Color)
admin.site.register(Products)
# Register your models here.

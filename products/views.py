from .serializers import CategorySerializer, ModelSerializer, ColorSerializer, ProductSerializer
from .models import Category, Color, Model, Products
from rest_framework import permissions, generics


class CategoryAPIView(generics.ListCreateAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryDetailAPIView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ColorAPIView(generics.ListCreateAPIView):
    queryset = Color.objects.all()
    serializer_class = ColorSerializer

    def get_serializer_context(self):
        context = super(ColorAPIView, self).get_serializer_context()
        context.update({
            'user': self.request.user
        })
        return context


class ModelAPIView(generics.ListCreateAPIView):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer


class ModelDetailAPIView(generics.RetrieveAPIView):
    queryset = Model.objects.all()
    serializer_class = ModelSerializer

    def get_serializer_context(self):
        context = super(ModelDetailAPIView, self).get_serializer_context()
        context.update({
            'user': self.request.user
        })
        return context


class ProductAPIView(generics.ListCreateAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductDetailApiView(generics.RetrieveUpdateAPIView):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer

    def get_serializer_context(self):
        context = super(ProductDetailApiView, self).get_serializer_context()
        context.update({
            'user': self.request.user
        })
        return context

# Create your views here.

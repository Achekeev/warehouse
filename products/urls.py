from django.urls import path
from .views import CategoryAPIView, ColorAPIView, ModelAPIView, ProductAPIView, ProductDetailApiView, \
    ModelDetailAPIView, CategoryDetailAPIView

urlpatterns = [
    path('categories/', CategoryAPIView.as_view(), name='categories'),
    path('categories/<int:pk>/', CategoryDetailAPIView.as_view(), name='category-details'),
    path('models/', ModelAPIView.as_view(), name='models'),
    path('models/<int:pk>/', ModelDetailAPIView.as_view(), name='model-details'),
    path('colors/', ColorAPIView.as_view(), name='color'),
    path('products/', ProductAPIView.as_view(), name='products'),
    path('products/<int:pk>/', ProductDetailApiView.as_view(), name='product-detail'),

]

from django.db import models
from users.models import Staff


class Category(models.Model):
    category_name = models.CharField(verbose_name='Категория', max_length=255)

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.category_name


class Model(models.Model):
    category = models.ForeignKey('Category', verbose_name='Категория', on_delete=models.CASCADE)
    model_name = models.CharField(verbose_name='Модель', max_length=255)

    class Meta:
        verbose_name = 'Model'
        verbose_name_plural = 'Models'

    def __str__(self):
        return self.model_name


class Color(models.Model):
    color_name = models.CharField(verbose_name='Цвет', max_length=255)

    class Meta:
        verbose_name = 'Color'
        verbose_name_plural = 'Colors'

    def __str__(self):
        return self.color_name


class Products(models.Model):
    category = models.ForeignKey('Category', verbose_name='Категория', on_delete=models.CASCADE)
    model = models.ForeignKey('Model', verbose_name='Модель', on_delete=models.CASCADE)
    color = models.ForeignKey('Color', verbose_name='Цвет', on_delete=models.CASCADE)
    amount = models.IntegerField(verbose_name='Количество', null=True)
    image = models.ImageField(upload_to='media/prod_pics', verbose_name='Фото', blank=True, null=True)
    date_added = models.DateTimeField(auto_now_add=True, verbose_name='Дата', null=True)
    responsible = models.ForeignKey('users.Staff', verbose_name='Ответственный', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

    def __str__(self):
        return self.category.category_name


# Create your models here.

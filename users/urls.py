from django.urls import path
from .views import StaffAPIView, StaffDetailApiView

urlpatterns = [
    path('staff/', StaffAPIView.as_view(), name='staff'),
    path('staff/<int:pk>/', StaffDetailApiView.as_view(), name='staff-detail'),
]

from django.shortcuts import render
from .models import Staff
from .serializers import StaffSerializer
from rest_framework import viewsets, generics
from rest_framework.authentication import TokenAuthentication
from rest_framework import permissions
from rest_framework import filters


class StaffAPIView(generics.ListCreateAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer
    # permissions = (,)


class StaffDetailApiView(generics.RetrieveUpdateAPIView):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

    def get_queryset(self):
        return self.queryset


# Create your views here.

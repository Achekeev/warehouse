from django.db import models


class Staff(models.Model):
    username = models.CharField(verbose_name='ФИО', max_length=255, null=True)
    email = models.EmailField(unique=True)
    phone_number = models.CharField(verbose_name='Телефон', max_length=100, null=True)
    position = models.CharField(verbose_name='Должность', max_length=255, null=True)

    class Meta:
        verbose_name = 'Staff'
        verbose_name_plural = 'Staff'

    def __str__(self):
        return self.username


# Create your models here.
